import sys
sys.path.append('./Deep-MVLM/')
import os
import argparse
from parse_config import ConfigParser
import deepmvlm

import numpy as np
import math

base_dir = './Deep-MVLM/'
config_dir = os.path.join(base_dir,'configs-MRI', 'DTU3D-geometry-depth.json')
#config_dir = os.path.join(base_dir,'configs', 'DTU3D-geometry+depth.json')

def get_config(param_list=[]):
    args = argparse.ArgumentParser()
    
    args.add_argument('--config', type=str)
    args.add_argument('--device', type=str)
    args.add_argument('--name', type=str)
    
    # For hacking the argparse.parse_args() function
    param = ["--config", config_dir, 
             "--device", "all", 
             "--name", None]
    
    global_config = ConfigParser(args=args, param_list=param)
    return global_config

def compute_mvlm(patient_id: str, obj_file_path: str):
    config = get_config()
    landmarks_dir = os.path.join('./data/mvlm', f'{patient_id}_landmarks.xyz')

    model = deepmvlm.DeepMVLM(config)
    landmarks = model.predict_one_file(obj_file_path)
    initial_transformation = model.get_pre_transformation_matrix(obj_file_path)
    model.write_landmarks_as_text(landmarks, landmarks_dir)

    return landmarks_dir, initial_transformation

def compute_pre_transformations(obj_file_path: str):
    config = get_config()
    model = deepmvlm.DeepMVLM(config)
    initial_transformation = model.get_pre_transformation_matrix(obj_file_path)
    return initial_transformation

if __name__ == '__main__':

    #face_object_test = os.path.join(base_dir, 'assets/rotated_ferreira_meshlab.obj')
    #face_object_test = os.path.join(base_dir, 'assets/ferreira_meshlab.obj')
    #face_object_test = os.path.join(base_dir, '/home/aang/Desktop/ferreira.stl')
    face_object_test = '/home/aang/Desktop/ferreira_meshlab.obj'

    config = get_config()

    dm = deepmvlm.DeepMVLM(config)
    landmarks = dm.predict_one_file(face_object_test)
    initial_transformation = dm.get_pre_transformation_matrix(face_object_test)
    print(f'T1 pre-transformations: {initial_transformation}')

    #name_lm_vtk = os.path.splitext(face_object_test)[0] + '_landmarks.vtk'
    #name_lm_txt = os.path.splitext(face_object_test)[0] + '_landmarks.xyz'
    #dm.write_landmarks_as_vtk_points(landmarks, name_lm_vtk)
    #dm.write_landmarks_as_text(landmarks, name_lm_txt)

    dm.visualise_mesh_and_landmarks(face_object_test, landmarks)








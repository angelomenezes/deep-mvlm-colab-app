import argparse
from parse_config import ConfigParser
import deepmvlm
import os

import numpy as np
import math

# The parameterization of the DL model is made using JSON configuration files 
# on the 'configs-MRI' folder.
# These config files specify which DL model to be used and the initial arrangements 
# for the 3D objects in order to process files in a standard pose which is a requirement 
# for the Deep-MVLM model.

face_object_1 = 'assets/ferreira_face.obj'
config_1 = os.path.join('configs-MRI', '(Scaled) DTU3D-geometry-depth.json')
'''
(Scaled) DTU3D-geometry-depth.json
"pre-align":{
		"align_center_of_mass" : false,
		"rot_x": 0
		"rot_y": 0,
		"rot_z": 0,
		"scale": 800,
		"write_pre_aligned": true
}
'''

face_object_2 = 'assets/ferreira_meshlab.obj'
config_2 = os.path.join('configs-MRI', 'DTU3D-geometry-depth.json')
'''
DTU3D-geometry-depth.json
"pre-align": {
		"align_center_of_mass" : true,
		"rot_x": 270,
		"rot_y": 0,
		"rot_z": 0,
		"scale": 1,
		"write_pre_aligned": true
	}
'''
device = 'all'

if __name__ == '__main__':

	args_1 = argparse.ArgumentParser()
	args_1.add_argument('-c', '--config', default=config_1, type=str)
	args_1.add_argument('-d', '--device', default=device, type=str)
	args_1.add_argument('-n', '--name', default=face_object_1, type=str)
	global_config = ConfigParser(args_1)

	dm_1 = deepmvlm.DeepMVLM(global_config)
	landmarks_1 = dm_1.predict_one_file(face_object_1)

	#name_lm_vtk = os.path.splitext(face_object_1)[0] + '_landmarks.vtk'
	#name_lm_txt = os.path.splitext(face_object_1)[0] + '_landmarks.xyz'
	#dm_1.write_landmarks_as_vtk_points(landmarks_1, name_lm_vtk)
	#dm_1.write_landmarks_as_text(landmarks_1, name_lm_txt)

	dm_1.visualise_mesh_and_landmarks(face_object_1, landmarks_1)

	args_2 = argparse.ArgumentParser()
	args_2.add_argument('-c', '--config', default=config_2, type=str)
	args_2.add_argument('-d', '--device', default=device, type=str)
	args_2.add_argument('-n', '--name', default=face_object_2, type=str)
	global_config = ConfigParser(args_2)

	dm_2 = deepmvlm.DeepMVLM(global_config)
	landmarks_2 = dm_2.predict_one_file(face_object_2)

	#name_lm_vtk = os.path.splitext(face_object_2)[0] + '_landmarks.vtk'
	#name_lm_txt = os.path.splitext(face_object_2)[0] + '_landmarks.xyz'
	#dm_2.write_landmarks_as_vtk_points(landmarks_2, name_lm_vtk)
	#dm_2.write_landmarks_as_text(landmarks_2, name_lm_txt)
	
	dm_2.visualise_mesh_and_landmarks(face_object_2, landmarks_2)




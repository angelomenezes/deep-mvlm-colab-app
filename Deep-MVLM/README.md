This folder is a clone of the Deep-MVLM original repository. We only simplified this README.md and included some testing scripts.

For getting the face landmarks from the 3D object, check the script 
[test_landmarks_NKAR.py](test_landmarks_NKAR.py). 
All 3D object samples and outputs are on the 'assets' folder. 

# Deep learning based 3D landmark placement
A tool for accurately placing 3D landmarks on 3D facial scans based on the paper [Multi-view Consensus CNN for 3D Facial Landmark Placement](https://arxiv.org/abs/1910.06007).

![Overview](art/deep-mvlm-banner.png)

### Requirements

The code has been tested under Windows 10 both with a GPU enabled (Titan X) computer and without a GPU (works but slow). It has been tested with the following dependencies

- Python 3.7
- Pytorch 1.2
- vtk 8.2
- libnetcdf 4.7.1 (needed by vtk)
- imageio 2.6
- matplotlib 3.1.1
- scipy 1.3.1
- scikit-image 0.15
- tensorboard 1.14
- absl-py 0.8


## Getting started
The easiest way to use Deep-MVLM is to use the pre-trained models to place landmarks on your meshes. To place the [DTU-3D](https://bmcmedimaging.biomedcentral.com/articles/10.1186/1471-2342-14-35) landmarks on a mesh try:

```
python predict.py --c configs/DTU3D-RGB.json --n assets/testmeshA.obj
```
This should create two landmarks files (a .vtk file and a .txt) file in the assets directory and also show a window with a face mesh with landmarks as (its a 3D rendering that can be manipulated with the mouse):

![Predicted output](art/PredictWindow.png)

## Supported formats and types

The framework can place landmarks on surface without textures, with textures and with vertex coloring. The supported formats are:

- **OBJ** textured surfaces (including multi textures), non-textured surfaces
- **WRL** textured surfaces (only single texture), non-textured surfaces
- **VTK** textured surfaces (only single texture), vertex colored surfaces, non-textured surfaces
- **STL** non-textured surfaces
- **PLY** non-textured surfaces


### Rendering types
The type of 3D rendering used is specified in the **image_channels** setting in the JSON configuration file. The options are:
- **geometry** pure geometry rendering without texture (1 image channel)
- **depth** depth rendering (the z-buffer) similar to range scanners like the Kinect (1 image channel)
- **RGB** texture rendering (3 image channels)
- **RGB+depth** texture plus depth rendering (3+1=4 image channels)
- **geometry+depth** geometry plus depth rendering (1+1=2 image channels)

### Pre-trained networks

The algorithm comes with pre-trained networks for the landmark sets **DTU3D** consisting of 73 landmarks that are described in this [paper](https://bmcmedimaging.biomedcentral.com/articles/10.1186/1471-2342-14-35) and [here](docs/DTU-3D_landmark_info.txt) and the landmark set from **BU-3DFE** described further down.

## Predict landmarks on a single scan

First determine what landmark set you want to place. Either **DTU3D** or **BU-3DFE**. Secondly, choose the rendering type suitable for your scan. Here are some recommendations:

- **surface with RGB texture** use **RGB+depth** or **RGB**
- **surface with vertex colors** use **RGB+depth** or **RGB**
- **surface with no texture** use **geometry+depth**, **geometry** or **depth**

Now you can choose the JSON config file that fits your need. For example **configs/DTU3D-RGB+depth.json**. Finally, do the prediction:

```
python predict.py --c configs/DTU3D-RGB+depth.json --n yourscan
```

## Predict landmarks on a directory with scans

Select a configuration file following the approach above and do the prediction:

```
python predict.py --c configs/DTU3D-RGB+depth.json --n yourdirectory
```

where **yourdirectory** is a directory (or directory tree) containing scans. It will process all **obj, wrl, vtk, stl** and **ply** files.

## Predict landmarks on a file with scan names

Select a configuration file following the approach above and do the prediction:

```
python predict.py --c configs/DTU3D-RGB+depth.json --n yourfile.txt
```

where **yourfile.txt** is a text file containing names of scans to be processed.

## Specifying a pre-transformation

The algorithm expects that the face has a general placement and orientation. Specifically, that the scan is centered around the origin and that the nose is pointing in the z-direction and the up of the head is aligned with the y-axis as seen here:

![coord-system](art/coord_system.png)

In order to re-align a scan to this system, a section of the JSON configuration file can be modified:
```
"pre-align": {
	"align_center_of_mass" : true,
	"rot_x": 0,
	"rot_y": 0,
	"rot_z": 180,
	"scale": 1,
	"write_pre_aligned": true
}
```
Here the scan is first aligned so the center-of-mass of the scan is aligned to the origo. Secondly, it is rotated 180 degrees around the z-axis. The rotation order is z-x-y. This will align this scan:

![mri-coord-system](art/mr_org_coord.png)

so it is aligned for processing and the result is:

![mri-results3](art/mr-head-landmarks3.png)

this configuration file can be found as [configs/DTU3D-depth-MRI.json](configs/DTU3D-depth-MRI.json)

## How to use the framework in your own code

Detect 3D landmarks in a 3D facial scan

```Python
import argparse
from parse_config import ConfigParser
import deepmvlm
from utils3d import Utils3D

dm = deepmvlm.DeepMVLM(config)
landmarks = dm.predict_one_file(file_name)
dm.write_landmarks_as_vtk_points(landmarks, name_lm_vtk)
dm.write_landmarks_as_text(landmarks, name_lm_txt)
dm.visualise_mesh_and_landmarks(file_name, landmarks)
```

The full source (including how to read the JSON config files) is [predict.py](predict.py)


## Examples

The following examples use data from external sources.

### Artec3D Eva example

Placing landmarks on an a scan produced using an [Artec3D Eva 3D scanner](https://www.artec3d.com/portable-3d-scanners/artec-eva) can be done like this:

- download the example [head scan](https://www.artec3d.com/3d-models/face) in obj format
- then:
```
python predict.py --c configs\DTU3D-RGB_Artec3D.json --n Pasha_guard_head.obj
```
![Artec3D](art/artec3d-eva-pasha.png)

- download the example [man bust](https://www.artec3d.com/3d-models/head) in obj format
- then:
```
python predict.py --c configs\DTU3D-depth.json --n man_bust.obj
```
![Artec3D](art/artec3d-eva-man-bust.png)


### Using Multiple GPUs

Multi-GPU training and evaluation can be used by setting `n_gpu` argument of the config file to a number greater than one. If configured to use a smaller number of GPUs than available, n devices will be used by default. To use a specific set of GPUs the command line option **--device** can be used:
```
python train.py --device 2,3 --c config.json
```
The program check if a GPU is present and if it has the required CUDA capabilities (3.5 and up). If not, the CPU is used - will be slow but still works.

## License
Deep-MVLM is released under the MIT license. See the [LICENSE file](LICENSE) for more details.

## Credits
This project is based on the PyTorch template  [pytorch-template](https://github.com/victoresque/pytorch-template) by [Victor Huang](https://github.com/victoresque)

We gratefully acknowledge the support of NVIDIA Corporation with the donation of the Titan Xp GPU used for this research.
